use std::net::Ipv4Addr;

use axum::{
    extract::{FromRef, Query, State},
    response::{IntoResponse, Redirect},
    routing::get,
    Router,
};
use axum_template::{engine::Engine, RenderHtml};
use handlebars::Handlebars;
use serde::{Deserialize, Serialize};
use serde_json::json;
use tokio::net::TcpListener;

type AppEngine = Engine<Handlebars<'static>>;

#[derive(Clone, Serialize)]
struct Contact {
    id: u32,
    first: String,
    last: String,
    email: String,
    phone: String,
}

#[derive(Clone, FromRef)]
struct AppState {
    engine: AppEngine,
    contacts: Vec<Contact>,
}

#[derive(Deserialize)]
struct ContactsParams {
    q: Option<String>,
}

async fn contacts(
    State(state): State<AppState>,
    query: Query<ContactsParams>,
) -> impl IntoResponse {
    let contact = query
        .q
        .as_ref()
        .map(|q| state.contacts.iter().find(|c| &c.first == q).cloned())
        .unwrap_or_default();
    let contacts = contact
        .clone()
        .map(|c| vec![c])
        .unwrap_or(state.contacts.clone());
    let q = query.q.clone().unwrap_or_default();

    RenderHtml(
        "contacts",
        state.engine.clone(),
        json!({"query": q, "contacts": contacts}),
    )
}

async fn index() -> impl IntoResponse {
    Redirect::permanent("/contacts")
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut hbs = Handlebars::new();
    hbs.register_template_string("contacts", include_str!("index.html"))
        .unwrap();

    let router = Router::new()
        .route("/", get(index))
        .route("/contacts", get(contacts))
        .with_state(AppState {
            engine: Engine::from(hbs),
            contacts: vec![
                Contact {
                    id: 0,
                    first: "Bob".to_owned(),
                    last: "Smith".to_owned(),
                    email: "bob@smith.xyz".to_owned(),
                    phone: "01234567890".to_owned(),
                },
                Contact {
                    id: 1,
                    first: "Jane".to_owned(),
                    last: "Doe".to_owned(),
                    email: "janedoe@example.com".to_owned(),
                    phone: "98765432101".to_owned(),
                },
            ],
        });
    let listener = TcpListener::bind((Ipv4Addr::new(127, 0, 0, 1), 4444)).await?;

    axum::serve(listener, router).await?;

    Ok(())
}
